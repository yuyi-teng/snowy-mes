package vip.xiaonuo.modular.suppperson.Result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.suppperson.entity.SuppPerson;
@Data
public class SuppPersonResult extends SuppPerson {
    /**
     * 供应商资料名称
     */
    @Excel(name = "供应商资料名称")
    private String suppDataName;
}