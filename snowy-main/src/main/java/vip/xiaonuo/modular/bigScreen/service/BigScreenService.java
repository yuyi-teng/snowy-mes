package vip.xiaonuo.modular.bigScreen.service;

import vip.xiaonuo.modular.bigScreen.param.WorkStepOrderParam;
import vip.xiaonuo.modular.bigScreen.result.*;

import java.util.List;

public interface BigScreenService {
    List<RejectsResult> queryRejects();
    List<WorkOrderProResult> queryWorkOrderPro();
    TodayAndConductResult todayAndConduct();
    List<WorkOrderAlertResult> queryWorkOrderAlert();
    List<WorkStepOrderResult> queryWorkStepOrder(WorkStepOrderParam workStepOrderParam);
}
