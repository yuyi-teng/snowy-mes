package vip.xiaonuo.modular.bigScreen.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.bigScreen.param.WorkStepOrderParam;
import vip.xiaonuo.modular.bigScreen.service.BigScreenService;
import vip.xiaonuo.modular.invOut.param.InvOutParam;

import javax.annotation.Resource;

@RestController
public class BigScreenController {
    @Resource
    private BigScreenService bigScreenService;

    @GetMapping("/bigScreen/queryRejects")
    @BusinessLog(title = "不良品查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData queryRejects() {
        return new SuccessResponseData(bigScreenService.queryRejects());
    }

    @GetMapping("/bigScreen/queryWorkOrderPro")
    @BusinessLog(title = "工单产出", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData queryWorkOrderPro() {
        return new SuccessResponseData(bigScreenService.queryWorkOrderPro());
    }

    @GetMapping("/bigScreen/odayAndConduct")
    @BusinessLog(title = "今日和在制", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData todayAndConduct() {
        return new SuccessResponseData(bigScreenService.todayAndConduct());
    }

    @GetMapping("/bigScreen/queryWorkOrderAlert")
    @BusinessLog(title = "工单预警", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData queryWorkOrderAlert() {
        return new SuccessResponseData(bigScreenService.queryWorkOrderAlert());
    }

    @GetMapping("/bigScreen/workStepOrder")
    @BusinessLog(title = "工序任务", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData queeryWorkStepOrder(WorkStepOrderParam workStepOrderParam) {
        return new SuccessResponseData(bigScreenService.queryWorkStepOrder(workStepOrderParam));
    }

}
