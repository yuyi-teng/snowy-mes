package vip.xiaonuo.modular.customcode.enums;

/**
 *
 * 不去空格白名单
 * 即一张表可能存在多种规则的情况
 *
 * @author sxy
 * @Date 14:22 2022/6/30
 **/
public enum WhitelistEnum {
    DW_INV
}
