package vip.xiaonuo.modular.pro.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.pro.entity.Pro;
import vip.xiaonuo.modular.protype.entity.ProType;
@Data
public class ProPageResult extends Pro {
    /**
     * 父类型名称
     */
    @Excel(name = "父类型名称")
    private String proTypeName;

}
