package vip.xiaonuo.modular.workreport.enums;

import lombok.Getter;

@Getter
public enum WorkReportApprovalEnum {
    /*
    从报工添加
    * */
    APPROVAL_YES(1,"已审批"),
    APPROVAL_NO(0,"未审批")
    ;

    private final Integer code;

    private final String message;

    WorkReportApprovalEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
