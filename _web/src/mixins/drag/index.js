import Vue from 'vue'
import VueDraggableResizable from 'vue-draggable-resizable'

export default {
  data() {
    return {
      comp: {
        header: {
          cell: (h, props, children) => {
            const { key, ...restProps } = props
            const col = this.columns.find(col => {
              const k = col.dataIndex || col.key
              return k === key
            })

            if (!col || !col.width) {
              return h('th', { ...restProps }, [...children])
            }

            const dragProps = {
              key: col.dataIndex || col.key,
              class: 'table-draggable-handle',
              attrs: {
                w: 10,
                x: col.width,
                z: 1,
                axis: 'x',
                draggable: true,
                resizable: false
              },
              on: {
                dragging: (x, y) => {
                  col.width = Math.max(x, 1)
                }
              }
            }
            const drag = h('vue-draggable-resizable', { ...dragProps })
            return h('th', { ...restProps, class: 'resize-table-th' }, [...children, drag])
          }
        }
      }
    }
  },
  created() {
    Vue.component('vue-draggable-resizable', VueDraggableResizable)
  },
  methods: {
    draggingState(columns) {
      const draggingMap = {}
      columns.forEach(col => {
        draggingMap[col.key] = col.width
      })
      return Vue.observable(draggingMap)
    }
  }
}
// 使用方法：
// 1、引入
//   import dragMixin from '@/mixins/drag/index'
//   import '@/mixins/drag/drag.css'
//   mixins: [ dragMixin ],
// 2、为s-table标签添加属性
// :components="this.comp"
// bordered
// 3、表头 columns属性
// 添加：width: 100,
// 添加：ellipsis: true,
// 操作注释掉：dataIndex: 'action',
