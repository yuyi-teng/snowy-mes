import { axios } from '@/utils/request'

/**
 * 查询出库单
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
export function invOutPage (parameter) {
  return axios({
    url: '/invOut/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 出库单列表
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
export function invOutList (parameter) {
  return axios({
    url: '/invOut/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加出库单
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
export function invOutAdd (parameter) {
  return axios({
    url: '/invOut/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑出库单
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
export function invOutEdit (parameter) {
  return axios({
    url: '/invOut/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除出库单
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
export function invOutDelete (parameter) {
  return axios({
    url: '/invOut/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出出库单
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
export function invOutExport (parameter) {
  return axios({
    url: '/invOut/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
