import { axios } from '@/utils/request'

/**
 * 查询 供应商资料
 *
 * @author ZJK
 * @date 2022-08-04 17:01:17
 */
export function suppDataPage (parameter) {
  return axios({
    url: '/suppData/page',
    method: 'get',
    params: parameter
  })
}

/**
 *  供应商资料列表
 *
 * @author ZJK
 * @date 2022-08-04 17:01:17
 */
export function suppDataList (parameter) {
  return axios({
    url: '/suppData/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加 供应商资料
 *
 * @author ZJK
 * @date 2022-08-04 17:01:17
 */
export function suppDataAdd (parameter) {
  return axios({
    url: '/suppData/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑 供应商资料
 *
 * @author ZJK
 * @date 2022-08-04 17:01:17
 */
export function suppDataEdit (parameter) {
  return axios({
    url: '/suppData/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除 供应商资料
 *
 * @author ZJK
 * @date 2022-08-04 17:01:17
 */
export function suppDataDelete (parameter) {
  return axios({
    url: '/suppData/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出 供应商资料
 *
 * @author ZJK
 * @date 2022-08-04 17:01:17
 */
export function suppDataExport (parameter) {
  return axios({
    url: '/suppData/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
