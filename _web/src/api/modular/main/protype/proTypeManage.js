import { axios } from '@/utils/request'

/**
 * 查询产品类型表
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
export function proTypePage (parameter) {
  return axios({
    url: '/proType/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 产品类型表列表
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
export function proTypeList (parameter) {
  return axios({
    url: '/proType/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加产品类型表
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
export function proTypeAdd (parameter) {
  return axios({
    url: '/proType/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑产品类型表
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
export function proTypeEdit (parameter) {
  return axios({
    url: '/proType/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除产品类型表
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
export function proTypeDelete (parameter) {
  return axios({
    url: '/proType/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出产品类型表
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
export function proTypeExport (parameter) {
  return axios({
    url: '/proType/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
  /**
   * 获取类型树
   *
   * @author lixingda
   * @date 2022-05-23 14:01:44
   */
export function getProTree (parameter) {
  return axios({
    url: '/proType/tree',
    method: 'get',
    params: parameter
  })
}
