import { axios } from '@/utils/request'

/**
 * 查询仓库管理
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
export function wareHousePage (parameter) {
  return axios({
    url: '/wareHouse/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 仓库管理列表
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
export function wareHouseList (parameter) {
  return axios({
    url: '/wareHouse/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加仓库管理
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
export function wareHouseAdd (parameter) {
  return axios({
    url: '/wareHouse/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑仓库管理
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
export function wareHouseEdit (parameter) {
  return axios({
    url: '/wareHouse/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除仓库管理
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
export function wareHouseDelete (parameter) {
  return axios({
    url: '/wareHouse/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出仓库管理
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
export function wareHouseExport (parameter) {
  return axios({
    url: '/wareHouse/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 获取类型树
 *
 * @author czw
 * @date 2022-07-23 14:01:44
 */
export function getWareHouseTree (parameter) {
  return axios({
    url: '/wareHouse/tree',
    method: 'get',
    params: parameter
  })
}
